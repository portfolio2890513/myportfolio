import React from 'react';
import PictureSlide from '../../ui/PictureSlide/PictureSlide';
import Image from '../../../assets/image.png';
import Image2 from '../../../assets/image2.jpg';
import Group from '../../../assets/group.jpg';
import './career.scss';
import { useNavigate } from 'react-router-dom';
import IconWithName from '../../ui/IconWithName/IconWithName';
import Health from '../../../assets/healthcare.svg';
import Compensation from '../../../assets/compensation.svg';
import Family from '../../../assets/family.svg';
import { animate, motion } from 'framer-motion';
import { toast } from 'react-toastify';

const Career = () => {
  const navigate = useNavigate();
  const imgArr = [
    {
      image: Image,
      name: 'Image',
    },
    {
      image: Image2,
      name: 'Image2',
    },
    {
      image: Image,
      name: 'Image',
    },
    {
      image: Image2,
      name: 'Image2',
    },
    {
      image: Image,
      name: 'Image',
    },
    {
      image: Image2,
      name: 'Image2',
    },
    {
      image: Image,
      name: 'Image',
    },
    {
      image: Image2,
      name: 'Image2',
    },
  ];

  const imgIconArr = [
    {
      icon: Health,
      name: 'health',
      content: 'Healthcare & wellness benefits',
    },
    {
      icon: Family,
      name: 'health',
      content: 'Paid parental & family leave',
    },
    {
      icon: Compensation,
      name: 'health',
      content: 'Competitive compensation',
    },
    {
      icon: Health,
      name: 'health',
      content: 'Healthcare & wellness benefits',
    },
    {
      icon: Compensation,
      name: 'health',
      content: 'Competitive compensation',
    },
  ];
  return (
    <motion.div
      className="bg-black"
      initial={{ x: '-100vw' }}
      animate={{ x: '0' }}
      exit={{ opacity: 0, transition: { duration: 0.1 } }}
    >
      <div className=" flex flex-col text-center gap-y-8 pt-8">
        <div className="text-7xl text-white font-semibold">We're Shaping the IT Economy</div>
        <div className="font-medium w-[40%] self-center">
          We’re changing the way creators make money, build their businesses and share their content with the world.
          Join our team!
        </div>
        <div>
          <motion.button
            onClick={() => {
              toast.success('Stay Tunned we will be back !', {
                position: 'top-right',
              });
            }}
            style={{ backgroundColor: 'green', color: 'white' }}
            whileTap={{ scale: 0.85 }}
          >
            Search Jobs
          </motion.button>
        </div>
      </div>
      <div className="my-10">
        <PictureSlide imgArray={imgArr} />
      </div>
      <div className="grid grid-cols-12 text-2xl font-bold mt-14 ml-7">
        <div className="col-start-3 col-end-5">We’re growing fast!</div>
      </div>
      <div className="textContainer text-center align-middle text-2xl font-bold my-8">
        <div className="flex flex-col justify-between">
          <div className="text-6xl">25,000+</div>
          <div>Hired</div>
        </div>
        <div className="flex flex-col justify-between">
          <div className="text-6xl">500+</div>
          <div>Project Delivered</div>
        </div>
        <div className="flex flex-col justify-between">
          <div className="text-6xl">8.5 million+</div>
          <div>Followers</div>
        </div>
      </div>
      <div className="grpContainer bg-white">
        <div className="grid21"></div>
        <div className="grid22"></div>
        <div className="grid23 p-6">
          <div className="text-black text-4xl font-semibold">Our Moto is simple .....</div>
          <br />
          <div className="text-gray-500 text-xl">
            To revolutionize the creator economy by empowering video creators and entrepreneurs to build lucrative
            membership businesses around their content.
          </div>
          <br />
          <div className="text-gray-500 text-xl">That’s why we need the best and brightest to join our team.</div>
          <br />
          <div className="text-2xl text-gray-700 cursor-pointer" onClick={() => navigate('/home')}>
            Learn more about our company &#x2192;
          </div>
        </div>
        <div className="grid24">
          <img src={Group} alt="group" />
        </div>
        <div className="grid25"></div>
        <div className="grid26"></div>
      </div>
      <div className="benefitContainer bg-white py-8">
        <div className="grid31">
          <div className="text-black text-4xl font-semibold">Benefits & Perks</div>
          <br />
          <div className="text-gray-500 text-xl">
            We’re honored by the incredible minds that choose to work at Niveus and are fully committed to their
            happiness, wellness, and comfort.
          </div>
        </div>
        <div className="grid32"></div>
        <div className="grid33"></div>
        <div className="grid34">
          <IconWithName imgArr={imgIconArr} />
        </div>
      </div>
    </motion.div>
  );
};

export default Career;
