import React from 'react';
import './desktop-navbar.scss';
import Logo from '../../../assets/logo.svg';
import { useNavigate } from 'react-router-dom';

const DesktopNavbar = () => {
  const navigate = useNavigate();
  return (
    <nav className="navbar-desktop">
      <div onClick={() => navigate('/home')} className="cursor-pointer flex flex-row items-center gap-1">
        <img src={Logo} alt="logo" />
        <p className="text-yellow-100 text-lg font-bold">Niveus Solution</p>
      </div>
      <div>
        <ul>
          <li>
            <a href="/home">Home</a>
          </li>
          <li>
            <a href="/about">About</a>
            <ul class="submenu">
              <li>
                <a href="#">Material</a>
              </li>
              <li>
                <a href="#">Cost Effective</a>
              </li>
              <li>
                <a href="#">Color Combination</a>
              </li>
              <li>
                <a href="#">Nearby Location</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="/career">Career</a>
            <ul class="submenu">
              <li>
                <a href="#">Beds</a>
              </li>
              <li>
                <a href="#">Chairs</a>
              </li>
              <li>
                <a href="#">Cebinets</a>
              </li>
              <li>
                <a href="#">Desk & Table</a>
              </li>
              <li>
                <a href="#">Bookcase</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="/contact">Contact</a>
          </li>
        </ul>
      </div>
      <div>
        <div class="flex items-center md:order-2 space-x-1 md:space-x-2 rtl:space-x-reverse">
          <a
            href="#"
            class="text-gray-800 dark:text-white hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2 md:px-5 md:py-2.5 dark:hover:bg-gray-700 focus:outline-none dark:focus:ring-gray-800"
          >
            Login
          </a>
          <a
            href="/register"
            class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 md:px-5 md:py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          >
            Sign up
          </a>
        </div>
      </div>
    </nav>
  );
};

export default DesktopNavbar;
