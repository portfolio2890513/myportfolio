import React from 'react';
import './scroll.scss';
import Funny1 from '../../../assets/funny1.jpg';
import Funny2 from '../../../assets/funny2.jpg';
import Funny3 from '../../../assets/funny3.jpg';
import { motion, useScroll } from 'framer-motion';

const ScrollComponent = ({ scrollPosi }) => {
  return (
    <>
      <motion.div className="relative">
        <motion.div className="scrollContainer">
          <motion.div className="w-[50%] h-[50vh] bg-slate-500 sticky top-[25%] left-[25%]">
            {console.log(scrollPosi)}
            <div
              style={{
                width: '100%',
                height: (scrollPosi - 0.5) * 2 * 100 < 2 ? '0%' : `${(scrollPosi - 0.5) * 2 * 100}%`,
                backgroundColor: 'white',
              }}
            >
              <p
                className="absolute left-[20%] text-5xl font-bold "
                style={{
                  top: scrollPosi - 0.5 < 0.05 ? '0%' : `${(scrollPosi - 0.5) * 100}%`,
                  backgroundImage: `linear-gradient(to bottom,#f21515 0%,#155bf2 ${(scrollPosi - 0.5) * 2 * 100 <= 50 ? '100' : '0'}%,black 0%,green ${(scrollPosi - 0.5) * 2 * 100 > 50 ? '100' : '0'}%)`,
                  backgroundClip: 'text',
                  '-webkit-text-fill-color': 'transparent',
                }}
              >
                FRAMER MOTION
              </p>
            </div>
          </motion.div>
          <motion.div className="imgContainer">
            <motion.div className="grid1 p-4">
              <motion.div className="h-[40%] w-[50%] bg-black text-white">
                <img
                  src={Funny1}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
            <motion.div className="grid2 p-4">
              {' '}
              <motion.div className="h-[40%] w-[50%] bg-black text-white">
                <img
                  src={Funny3}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
            <motion.div className="grid3 p-4">
              {' '}
              <motion.div className="h-[40%] w-[50%] bg-black text-white">
                <img
                  src={Funny2}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
            <motion.div className="grid4 p-4">
              {' '}
              <motion.div className="h-[40%] w-[50%] bg-black text-white">
                <img
                  src={Funny3}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
            <motion.div className="grid5 p-4">
              {' '}
              <motion.div className="h-[40%] w-[50%] bg-black text-white">
                <img
                  src={Funny2}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
            <motion.div className="grid6 p-4">
              {' '}
              <motion.div className="h-[40%] w-[50%] bg-black text-white">
                <img
                  src={Funny1}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
            <motion.div className="grid7 p-4">
              {' '}
              <motion.div className="h-[50%] w-[50%] bg-black text-white">
                <img
                  src={Funny3}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
            <motion.div className="grid8 p-4">
              {' '}
              <motion.div className="h-[50%] w-[50%] bg-black text-white">
                <img
                  src={Funny2}
                  style={{ backgroundSize: 'cover', height: '100%', width: '100%' }}
                  sizes="cover"
                  alt="fuuny1"
                />
              </motion.div>
            </motion.div>
          </motion.div>
        </motion.div>
      </motion.div>
    </>
  );
};

export default ScrollComponent;
