import React, { useState } from 'react';
import RegLogo from '../../../assets/register.png';
import { motion } from 'framer-motion';
import { toast } from 'react-toastify';

const Register = () => {
  const [getStarted, setGetStarted] = useState(false);
  const [resetAnimation, setResetAnimation] = useState(false);
  const [isTransitionComplete, setIsTransitionComplete] = useState(false);
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    dateOfBirth: '',
    password: '',
    confirmPassword: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Add your form submission logic here
    console.log(formData);
  };

  const handleAnimationComplete = () => {
    if (!resetAnimation) {
      setIsTransitionComplete(true);
    }
  };

  return (
    <div className="flex max-w-[100%] w-[100vw] h-[100vh] bg-gradient-to-r from-blue-700 to-blue-300 px-[100px]">
      {!isTransitionComplete && (
        <motion.div
          initial={!resetAnimation ? { x: 0, opacity: 1 } : { x: -100, opacity: 0 }}
          animate={getStarted ? { x: -100, opacity: 0 } : resetAnimation ? { x: 0, opacity: 1 } : {}}
          transition={{ duration: 1 }}
          className={`w-[50vw] h-[100vh] flex flex-col justify-center items-center `}
          onAnimationComplete={handleAnimationComplete}
        >
          <div className="text-9xl font-bold text-yellow-500 py-12">Register!!</div>
          <div className="text-5xl font-bold text-white"> Be Part Of Our Journey</div>
          <div className="my-12">
            <motion.button
              onClick={() => {
                setGetStarted(true);
                if (!isTransitionComplete) {
                  setResetAnimation(false);
                }
              }}
              style={{ backgroundColor: 'white', color: '#60A5FA' }}
              whileTap={{ scale: 0.85 }}
            >
              Get Started
            </motion.button>
          </div>
        </motion.div>
      )}
      {isTransitionComplete && (
        <motion.div
          className={`w-[100vw] h-[100vh]`}
          initial={{ scale: 0 }}
          animate={{ scale: 1 }}
          transition={{ duration: 1 }}
        >
          <div className="flex w-[100vw]  h-[100vh] justify-center  items-center">
            <div className="max-w-md mx-auto p-8 bg-white rounded-md shadow-md">
              <form onSubmit={handleSubmit}>
                <div className="mb-4">
                  <label htmlFor="firstName" className="block text-gray-700 font-semibold mb-2">
                    First Name
                  </label>
                  <input
                    type="text"
                    id="firstName"
                    name="firstName"
                    value={formData.firstName}
                    onChange={handleChange}
                    className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                    required
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="lastName" className="block text-gray-700 font-semibold mb-2">
                    Last Name
                  </label>
                  <input
                    type="text"
                    id="lastName"
                    name="lastName"
                    value={formData.lastName}
                    onChange={handleChange}
                    className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                    required
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="email" className="block text-gray-700 font-semibold mb-2">
                    Email
                  </label>
                  <input
                    type="email"
                    id="email"
                    name="email"
                    value={formData.email}
                    onChange={handleChange}
                    className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                    required
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="dateOfBirth" className="block text-gray-700 font-semibold mb-2">
                    Date of Birth
                  </label>
                  <input
                    type="date"
                    id="dateOfBirth"
                    name="dateOfBirth"
                    value={formData.dateOfBirth}
                    onChange={handleChange}
                    className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                    required
                  />
                </div>
                <div className="mb-4">
                  <label htmlFor="password" className="block text-gray-700 font-semibold mb-2">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    name="password"
                    value={formData.password}
                    onChange={handleChange}
                    className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                    required
                  />
                </div>
                <div className="mb-6">
                  <label htmlFor="confirmPassword" className="block text-gray-700 font-semibold mb-2">
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    id="confirmPassword"
                    name="confirmPassword"
                    value={formData.confirmPassword}
                    onChange={handleChange}
                    className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                    required
                  />
                </div>
                <button
                  type="submit"
                  className="w-full bg-blue-500 text-white py-2 rounded-md hover:bg-blue-600 transition duration-300"
                  onClick={() => {
                    setGetStarted(false);
                    setIsTransitionComplete(false);
                    setResetAnimation(true);
                    toast.success('Stay Tunned we will be back !', {
                      position: 'top-right',
                    });
                  }}
                >
                  Register
                </button>
              </form>
            </div>
          </div>
        </motion.div>
      )}
      {!isTransitionComplete && (
        <motion.div
          className={`w-[50vw]  h-[100vh]  `}
          initial={!resetAnimation ? { x: 0, opacity: 1 } : { x: 100, opacity: 0 }}
          animate={getStarted ? { x: 100, opacity: 0 } : resetAnimation ? { x: 0, opacity: 1 } : {}}
          transition={{ duration: 1 }}
          onAnimationComplete={handleAnimationComplete}
        >
          <div className="flex align-bottom justify-end">
            <img className="h-[100vh]" src={RegLogo} alt={'reglogo'} />
          </div>
        </motion.div>
      )}
    </div>
  );
};

export default Register;
