import React from 'react'
import Health from '../../../assets/healthcare.svg'

const IconWithName = ({ imgArr }) => {
  return imgArr?.map((val) => (
    <div>
      <img src={val.icon} alt={val.name} height={96} width={96} />
      <br />
      <div className="font-bold text-xl text-black">{val.content}</div>
    </div>
  ))
}

export default IconWithName
