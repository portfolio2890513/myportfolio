import React from 'react'
import './pictureSlide.scss'
import Image from '../../../assets/image.png'
import Image2 from '../../../assets/image2.jpg'

const PictureSlide = ({ imgArray }) => {
  return (
    <div className="picContainer">
      {imgArray.map((val, i) => (
        <div key={val.name}>
          <img src={val.image} alt={val.name} />
        </div>
      ))}
      {/* <div className="grid-11">
        <img src={Image} alt="person" />
      </div>
      <div className="grid-12">
        <img src={Image2} alt="person" />
      </div>
      <div className="grid-13">
        <img src={Image} alt="person" />
      </div>
      <div className="grid-14">
        <img src={Image2} alt="person" />
      </div>
      <div className="grid-15">
        <img src={Image} alt="person" />
      </div>
      <div className="grid-16">
        <img src={Image2} alt="person" />
      </div>
      <div className="grid-17">
        <img src={Image} alt="person" />
      </div>
      <div className="grid-18">
        <img src={Image2} alt="person" />
      </div> */}
    </div>
  )
}

export default PictureSlide
