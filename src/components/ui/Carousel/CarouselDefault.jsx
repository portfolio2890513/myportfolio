import { Carousel } from "react-responsive-carousel"
import "react-responsive-carousel/lib/styles/carousel.min.css"
import "./carousel.scss"

export function CarouselDefault() {
  return (
    <div className="containerCarousel">
      <Carousel centerMode showStatus={false} showThumbs={false}>
        <div>
          <img src="https://images.unsplash.com/photo-1497436072909-60f360e1d4b1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2560&q=80" />
          {/* <p className="legend">Legend 1</p> */}
        </div>
        <div>
          <img src="https://images.unsplash.com/photo-1493246507139-91e8fad9978e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2940&q=80" />
          {/* <p className="legend">Legend 2</p> */}
        </div>
        <div>
          <img src="https://images.unsplash.com/photo-1518623489648-a173ef7824f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2762&q=80" />
          {/* <p className="legend">Legend 3</p> */}
        </div>
      </Carousel>
    </div>
  )
}
