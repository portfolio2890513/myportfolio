import React from "react"
import { MapContainer, TileLayer, useMap } from "react-leaflet"
import ContactForm from "./ContactForm"

const Contact = () => {
  return (
    <>
      <ContactForm />
    </>
  )
}

export default Contact
