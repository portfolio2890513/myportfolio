import { Outlet, useLocation, useOutlet } from 'react-router-dom';
import { Navbar } from '../navbar/Navbar';
import Footer from '../../ui/Footer/Footer';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SmoothScroll from '../../ui/SmoothScroll/SmoothScroll';
import { AnimatePresence } from 'framer-motion';
import React from 'react';

const Layout = () => {
  return (
    <>
      <SmoothScroll>
        <ToastContainer />
        <div className="h-screen w-screen">
          <div className="h-auto bg-gray-600">
            <Navbar />
          </div>
          <div className="bg-white-200">
            <div className="bg-white-600 flex justify-center items-center">
              <AnimatedOutlet />
            </div>
          </div>
          <div className="h-auto bg-green-200">
            <Footer />
          </div>
        </div>
      </SmoothScroll>
    </>
  );
};

const AnimatedOutlet = () => {
  const location = useLocation();
  const element = useOutlet();

  return (
    <AnimatePresence mode="wait" initial={true}>
      {element && React.cloneElement(element, { key: location.pathname })}
    </AnimatePresence>
  );
};

export default Layout;
