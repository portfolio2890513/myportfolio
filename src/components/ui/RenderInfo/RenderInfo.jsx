import React from 'react';
import InfoComponent from './InfoComponent';
import Image from '../../../assets/image.png';
import Image2 from '../../../assets/image2.jpg';
import './renderInfo.scss';

const RenderInfo = () => {
  return (
    <div className="flex flex-wrap gap-8 justify-center items-center sm:w-[100vw] sm:h-[100vh] infoContainer">
      <InfoComponent
        image={Image}
        name={'Sahail Raj'}
        position={'Web Developer'}
        content={`Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nisi
              accusantium asperiores necessitatibus iusto, provident expedita.`}
      />
      <InfoComponent
        image={Image2}
        name={'Swapnil Bodade'}
        position={'Web Developer'}
        content={`Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nisi
              accusantium asperiores necessitatibus iusto, provident expedita.`}
      />
      <InfoComponent
        image={Image}
        name={'Sahail Raj'}
        position={'Web Developer'}
        content={`Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nisi
              accusantium asperiores necessitatibus iusto, provident expedita.`}
      />
      <InfoComponent
        image={Image2}
        name={'Swapnil Bodade'}
        position={'Web Developer'}
        content={`Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nisi
              accusantium asperiores necessitatibus iusto, provident expedita.`}
      />
    </div>
  );
};

export default RenderInfo;
