import React from "react"
import RenderInfo from "../../ui/RenderInfo/RenderInfo"

const About = () => {
  return (
    <div>
      <RenderInfo />
    </div>
  )
}

export default About
