import { useRef, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import Logo from '../../../assets/logo.svg';
import Down from '../../../assets/down.svg';
import './navbar.scss';
import { AnimatePresence, motion } from 'framer-motion';
import { Squash as Hamburger } from 'hamburger-react';
import { useClickAway } from 'react-use';
import DesktopNavbar from './DesktopNavbar';

export const Navbar = () => {
  const navigate = useNavigate();
  const [isOpen, setOpen] = useState(false);
  const [subMenuOpen, setSubMenuOpen] = useState(false);
  const ref = useRef(null);
  const refUl = useRef(null);
  useClickAway(ref, () => setOpen(false));

  let style = ({ isActive, isPending, isTransitioning }) => {
    return {
      fontWeight: isActive ? 'bold' : '',
      color: isActive ? 'white' : 'black',
      viewTransitionName: isTransitioning ? 'slide' : '',
    };
  };
  const [isContactDropdownOpen, setIsContactDropdownOpen] = useState(false);

  const toggleContactDropdown = () => {
    setIsContactDropdownOpen(!isContactDropdownOpen);
  };
  const handleMouseEnter = () => {
    setIsContactDropdownOpen(true);
  };

  const handleMouseLeave = () => {
    setIsContactDropdownOpen(false);
  };
  const handleCloseContactDropdown = () => {
    setIsContactDropdownOpen(false);
  };

  let mobMneu = [
    { name: 'Home', href: '/home' },
    [
      { name: 'About', href: '/about' },
      { name: 'Swapnil', href: '/home' },
      { name: 'Sahail', href: '/home' },
    ],
    [
      { name: 'Career', href: '/career' },
      { name: 'Swapnil', href: '/home' },
      { name: 'Sahail', href: '/home' },
    ],
    { name: 'Login', href: '#' },
    { name: 'Sign Up', href: '/register' },
  ];

  return (
    <div className="relative w-full h-full px-4">
      <div className="hidden lg:block xl:block 2xl:block">
        <DesktopNavbar />
      </div>
      <div ref={ref} className="lg:hidden xl:hidden 2xl:hidden">
        <Hamburger toggled={isOpen} size={20} toggle={setOpen} />
        <AnimatePresence>
          {isOpen && (
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              transition={{ duration: 0.2 }}
              className="absolute top-[100%] left-0 navbar-m h-[100vh] w-[50vw] max-w-[100%] overflow-auto bg-black navbarUl"
            >
              <ul className="flex flex-col justify-between items-center h-[50%] pt-[10%]">
                {mobMneu.map((val, idx) =>
                  !Array.isArray(val) ? (
                    <motion.li
                      initial={{ scale: 0, opacity: 0 }}
                      animate={{ scale: 1, opacity: 1 }}
                      transition={{
                        type: 'spring',
                        stiffness: 260,
                        damping: 20,
                        delay: 0.1 + idx / 10,
                      }}
                    >
                      <a href={val.href}>{val.name}</a>
                    </motion.li>
                  ) : (
                    <motion.li
                      initial={{ scale: 0, opacity: 0 }}
                      animate={{ scale: 1, opacity: 1 }}
                      transition={{
                        type: 'spring',
                        stiffness: 260,
                        damping: 20,
                        delay: 0.1 + idx / 10,
                      }}
                    >
                      <p
                        className="flex flex-row justify-evenly items-center w-full"
                        // onClick={() => setSubMenuOpen(!subMenuOpen)}
                        onClick={(e) => {
                          let a = e.target.value;

                          var element = document.getElementsByClassName(val[0].name);
                          let b = element[0].classList;
                          element[0].classList.toggle('close');
                        }}
                      >
                        <a href={val[0].href}>{val[0].name}</a>
                        <img src={Down} alt="down" height={16} width={16} />
                      </p>
                      {/* {subMenuOpen && ( */}
                      <ul ref={refUl} className={val[0].name + ' close'}>
                        {val.slice(1, 3).map((val1) => (
                          <motion.li
                            initial={{ scale: 0, opacity: 0 }}
                            animate={{ scale: 1, opacity: 1 }}
                            transition={{
                              type: 'spring',
                              stiffness: 260,
                              damping: 20,
                              delay: 0.1 + idx / 10,
                            }}
                          >
                            <a href={val1.href}>{val1.name}</a>
                          </motion.li>
                        ))}
                      </ul>
                      {/* )} */}
                    </motion.li>
                  ),
                )}
              </ul>
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </div>
  );
};
