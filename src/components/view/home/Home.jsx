import { motion } from 'framer-motion';
import { CarouselDefault } from '../../ui/Carousel/CarouselDefault';
import HomeBackground from '../../ui/Home/HomeBackground';
import Intro from '../../ui/Intro/Intro';

const Home = () => {
  return (
    <motion.div
      className="w-full h-full"
      // initial={{ width: '0%' }}
      // animate={{ width: '100%' }}
      initial={{ x: '-100vw' }}
      animate={{ x: '0' }}
      exit={{ opacity: 0, transition: { duration: 0.1 } }}
    >
      <div className="w-full">
        <HomeBackground />
      </div>
    </motion.div>
  );
};

export default Home;
