import React from 'react'
import Logo from '../../../assets/logo.svg'
import Facebook from '../../../assets/facebook.svg'
import Twitter from '../../../assets/twitter.svg'
import { animate, motion } from 'framer-motion'
import { useState } from 'react'

const InfoComponent = ({ name, content, position, image }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [timeoutId, seTimeoutId] = useState([])
  const handleMouseEnter = () => {
    setIsOpen(true)
    timeoutId.forEach((val) => clearTimeout(val))
    timeoutId.pop()
  }

  const handleMouseLeave = () => {
    let timeout = setTimeout(() => {
      setIsOpen(false)
    }, 4000)
    timeoutId.push(timeout)
  }
  return (
    <>
      <motion.div
        transition={{ layout: { duration: 1, type: 'spring' } }}
        onClick={() => {
          setIsOpen(!isOpen)
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        style={{
          boxShadow:
            '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
        }}
        className="w-[260px] h-auto bg-blue-50 rounded-3xl flex flex-col justify-between p-4 my-4"
      >
        <motion.div layout="position" className="self-center">
          <img
            className="rounded-full h-[230px] mb-2"
            src={image}
            alt="Image"
          />
          <div className="text-xl text-black font-bold text-center align-top">
            {name}
          </div>
        </motion.div>
        {isOpen && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            transition={{ duration: 1 }}
          >
            <div>
              <br />
              <div className="text-black font-medium text-center align-middle">
                <i>{content}</i>
              </div>
              <br />
              <div className="text-xl text-black font-bold text-center align-top">
                {position}
              </div>
            </div>
            <br />
            <div className="flex flex-row justify-center items-center gap-3">
              <div>
                <img className="cursor-pointer" src={Logo} alt="logo" />
              </div>
              <div>
                <img
                  className="cursor-pointer"
                  src={Facebook}
                  alt="Facebook"
                  height={24}
                  width={24}
                />
              </div>
              <div>
                <img
                  className="cursor-pointer"
                  src={Twitter}
                  alt="Twitter"
                  height={24}
                  width={24}
                />
              </div>
              <div>
                <img className="cursor-pointer" src={Logo} alt="logo" />
              </div>
            </div>
          </motion.div>
        )}
      </motion.div>
    </>
  )
}

export default InfoComponent
