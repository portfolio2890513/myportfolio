import React from 'react';
import { motion } from 'framer-motion';

const ButtonAnimated = ({ text, bgColor = 'bg-blue-500', onclick, color = 'text-white', border = 'border-black' }) => {
  let buttonVariant = {
    hover: {
      scale: 1.1,
      transition: {
        repeat: 10,
        repeatType: 'reverse',
        duration: 0.5,
      },
    },
  };
  return (
    <motion.div>
      <motion.button
        onclick={onclick}
        variants={buttonVariant}
        whileHover="hover"
        className={'p-3 rounded-2xl border-2 align-middle text-center ' + bgColor + ' ' + color + ' ' + border}
      >
        {text}
      </motion.button>
    </motion.div>
  );
};

export default ButtonAnimated;
