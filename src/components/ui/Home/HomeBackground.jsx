import React, { useEffect, useRef, useState } from 'react';
import Comp from '../../../assets/comp.gif';
import './homeBackground.scss';
import { toast } from 'react-toastify';
import Right from '../../../assets/right.jpg';
import ReactPlayer from 'react-player';
import Demo from '../../../assets/video/demo.mp4';
import ButtonAnimated from '../ButtonAnimated/ButtonAnimated';
import ScrollComponent from '../ScrollComponent/ScrollComponent';
import { motion, useMotionValueEvent, useScroll } from 'framer-motion';
import ScrollAnimate from '../ScrollComponent/ScrollAnimate';
// import { useScroll, useScrolling } from 'react-use';

const HomeBackground = () => {
  const { scrollYProgress } = useScroll();
  const [scrollPosi, setScrollPosi] = useState(0);

  useMotionValueEvent(scrollYProgress, 'change', (latest) => {
    console.log('Page scroll: ', latest);
    setScrollPosi(latest);
  });
  const handleClick = () => {
    toast.success('Stay Tunned we will be back !', {
      position: 'top-right',
    });
  };
  return (
    <motion.div>
      <motion.div className="progress-bar" style={{ scaleX: scrollYProgress }} />
      <motion.div className="homeContainer">
        {/* <img className="grpImg" src={Grp} alt="grp" /> */}
        <motion.div className="p-8 flex flex-col justify-end items-start mb-16">
          <motion.div className="text-3xl sm:text-4xl md:text-5xl lg:text-7xl xl:text-7xl 2xl:text-7xl text-white font-semibold">
            We help your career to be recognizable
          </motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
          </motion.div>
          <br />
          <motion.div>
            <ButtonAnimated text={'Learn More'} bgColor="bg-green-500" onclick={handleClick} />
          </motion.div>
        </motion.div>

        <motion.div></motion.div>
      </motion.div>
      <motion.div className="compContainer">
        <motion.div className="grid41">
          <motion.div className="text-5xl text-white font-semibold">Quality Product</motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
          </motion.div>
        </motion.div>
        <motion.div className="grid42">
          <motion.div className="text-5xl text-white font-semibold">Growth Stratergy</motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
          </motion.div>
        </motion.div>
        <motion.div className="grid43">
          <motion.div className="text-5xl text-white font-semibold">Competetive Prize</motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
          </motion.div>
        </motion.div>
        <motion.div className="grid44 bg-white text-black">
          <motion.div>
            <img src={Comp} alt="comp" height={76} width={76} />
          </motion.div>
          <br />
          <motion.div className="text-5xl font-semibold">About Company</motion.div>
          <br />
          <motion.div className="font-medium self-center w-[100%] lg:w-[40%] xl:w-[40%] 2xl:w-[40%]">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Error enim qui sunt explicabo temporibus officia
            repellat quas distinctio deserunt est delectus dolorum dignissimos repellendus, perferendis, reiciendis
            blanditiis quasi id ratione.
          </motion.div>
          <br />
          <motion.div>
            <ButtonAnimated text={'Our Work'} bgColor="bg-green-500" color="text-black" onclick={handleClick} />
          </motion.div>
        </motion.div>
      </motion.div>
      <motion.div className="shortIntroContainer">
        <motion.div className="grid51 py-[10vh]">
          <motion.div className="bg-gray-600 h-[100%]">
            <motion.div className="h-[100%] overflow-hidden rounded-full">
              <img src={Right} />
            </motion.div>
          </motion.div>
        </motion.div>
        <motion.div className="grid52 flex flex-col justify-center items-center bg-blue-400 my-[10vh] px-[5vw]">
          <motion.div className="text-4xl text-white font-bold text-left">
            Doing The Right Thing, At The Right Time
          </motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
          </motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
            laborum.
          </motion.div>
          <br />
          <motion.div className="self-start ">
            <ButtonAnimated
              text={'Learn More'}
              bgColor="bg-blue-400"
              border="border-current"
              color="text-current"
              onclick={handleClick}
            />
          </motion.div>
        </motion.div>
        <motion.div className="grid53 flex flex-col justify-center items-start bg-blue-400 my-[14vh] px-[5vw]">
          <motion.div className="text-4xl text-white font-bold text-left">What our leaders want to say</motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
          </motion.div>
          <br />
          <motion.div className="font-medium self-center">
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
            laborum.
          </motion.div>
          <br />
          <motion.div className="self-start">
            <ButtonAnimated
              text={'Learn More'}
              bgColor="bg-blue-400"
              border="border-current"
              color="text-current"
              onclick={handleClick}
            />
          </motion.div>
        </motion.div>
        <motion.div className="grid54">
          <motion.div className="h-[100%] pb-[15vh]">
            <ReactPlayer height={'100%'} width={'100%'} url={Demo} controls={true} />{' '}
          </motion.div>
        </motion.div>
      </motion.div>
      {/* <ScrollComponent scrollPosi={scrollPosi > 0.5 ? scrollPosi : 0} /> */}
      {/* <ScrollAnimate scrollPosi={scrollPosi > 0.5 ? scrollPosi : 0} /> */}
    </motion.div>
  );
};

export default HomeBackground;
