import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx';
import './index.css';
import { Navigate, RouterProvider, createBrowserRouter } from 'react-router-dom';
import Layout from './components/view/home/Layout.jsx';
import Contact from './components/view/contact/Contact.jsx';
import About from './components/view/about/About.jsx';
import Career from './components/view/career/Career.jsx';
import Home from './components/view/home/Home.jsx';
import Register from './components/view/register/Register.jsx';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    errorElement: <div>404 ERROR </div>,
    children: [
      { index: true, element: <Navigate to="/home" replace /> },
      {
        path: '/contact',
        element: <Contact />,
      },
      {
        path: '/home',
        element: <Home />,
      },
      {
        path: '/about',
        element: <About />,
      },
      {
        path: '/career',
        element: <Career />,
      },
      {
        path: '/register',
        element: <Register />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router}></RouterProvider>
  </React.StrictMode>,
);
