import React from 'react'
import './contact-form.scss'
import RenderMap from '../../ui/Map/RenderMap'
import RenderIcon from '../../ui/RenderIcon/RenderIcon'
import Address from '../../../assets/address.gif'
import Call from '../../../assets/call.gif'
import Email from '../../../assets/email.gif'
import Fax from '../../../assets/fax.gif'

const ContactForm = () => {
  return (
    <div className="formContainer">
      <div className="grid-1">
        <RenderIcon
          icon={Address}
          content={'Buikdng no-8, udipi, Karnataka-502145, India'}
          name={'Our Main Office'}
        />
      </div>
      <div className="grid-2">
        <RenderIcon
          icon={Call}
          content={
            <>
              <p>022-25487896 1800-9899899899(Toll Free)</p>
            </>
          }
          name={'Phone Number'}
        />
      </div>
      <div className="grid-3 w-full h-full">
        <RenderMap />
      </div>
      <div className="grid-4">
        <RenderIcon icon={Fax} content={'1-234-567-8900'} name={'Fax'} />
      </div>
      <div className="grid-5">
        <RenderIcon
          icon={Email}
          content={'niveus@solution.gmail.com'}
          name={'Email'}
        />
      </div>
      <div className="grid-6 text-center flex flex-col justify-center items-center">
        <div className="text-6xl font-bold">Contact Us</div>
        <div className="p-4 bg-red-300 w-1/4 rounded-3xl"></div>
      </div>
    </div>
  )
}

export default ContactForm
