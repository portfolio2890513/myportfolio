import React from 'react'
import Address from '../../../assets/address.gif'

const RenderIcon = ({ icon, name, content }) => {
  return (
    <div className="flex flex-col justify-between items-center w-[300px] h-[300px] bg-white text-black p-4 text-center">
      <div>
        <img src={icon} alt="address" height={94} width={94} />
      </div>
      <div className="text-3xl font-bold">{name}</div>
      <div className="text-xl font-medium break-all">{content}</div>
    </div>
  )
}

export default RenderIcon
