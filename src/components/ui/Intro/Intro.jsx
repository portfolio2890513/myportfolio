import Person from "../../../assets/person.jpg"
const Intro = () => {
  return (
    <div className="bg-white-500 h-[auto] w-[100vw]">
      
      <div className="flex flex-row p-8 gap-x-5">
        <div className="bg-white-500 flex flex-col h-full w-[70%] text-8xl border-double border-8 border-black-800 rounded-3xl p-4 text-center">
          <div className="text-black ">Welcome to our site</div>
          <div className="text-purple-600 text-center">and</div>
          <div className="text-black text-center">Hire me</div>
          <div className="text-base text-black">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptas
            sint illo, laboriosam atque consequatur molestias cumque, dolore
            possimus quibusdam quo praesentium fugit fuga recusandae voluptate?
            Vel, magni pariatur! Ut, porro.
          </div>
          <div className="self-end text-base border right-0 border-yellow-200 w-[120px] rounded-3xl p-4 mt-2 cursor-pointer hover:bg-red-600 text-right">
            Read More
          </div>
        </div>
        <div className="h-full w-[30%] rounded-3xl overflow-hidden self-center">
          <img src={Person} alt="person" />
        </div>
      </div>
    </div>
  )
}

export default Intro
