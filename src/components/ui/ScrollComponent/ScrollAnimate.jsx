import React, { useEffect, useRef } from 'react';
import './scroll-animate.scss';
import { motion, useAnimation, useInView } from 'framer-motion';

const ScrollAnimate = ({ scrollPosi }) => {
  let arr = [
    'control1',
    'control1',
    'control2',
    'control2',
    'control3',
    'control3',
    'control4',
    'control4',
    'control5',
    'control5',
    'control6',
  ];
  const controls1 = useAnimation();
  const controls2 = useAnimation();
  const controls3 = useAnimation();
  const controls4 = useAnimation();
  let divVariant = {
    hidden: { y: '100vh', opacity: 0 },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        duration: 2,
        type: 'tween',
      },
    },
  };

  useEffect(() => {
    if (scrollPosi > 0.55) {
      controls1.start('visible');
    } else if (scrollPosi > 0.6625) {
      controls1.stop();
      controls2.start('visible');
    } else if (scrollPosi > 0.725) {
      controls3.start('visible');
    } else if (scrollPosi > 0.8875) {
      controls4.start('visible');
    }
  }, [scrollPosi]);

  const getAnimate = (val) => {
    switch (val) {
      case 'control1':
        return controls1;
      case 'control2':
        return controls2;
      case 'control3':
        return controls3;
      case 'control4':
        return controls4;

      default:
        break;
    }
  };

  return (
    <>
      <motion.div className="scrollAni  relative overflow-hidden">
        {arr.map((val, i) =>
          i == 10 ? (
            <Section i={i} isCenter={false} scrollPosi={scrollPosi}>
              <div
                style={{
                  width: '50%',
                  left: '25%',
                  height: '100%',
                  backgroundColor: 'white',
                  textAlign: 'center',
                  zIndex: -1,
                  top: scrollPosi - 0.5 < 0.05 ? '0%' : `${(scrollPosi - 0.6) * 2 * 100}%`,
                }}
                className={`grid11 text-6xl font-bold text-black bg-red text-center]`}
              >
                Why us ?
              </div>
            </Section>
          ) : (
            <Section i={i} scrollPosi={scrollPosi}>
              <CardRender width={i == 8 || i == 9 ? '80%' : '50%'} />
            </Section>
          ),
        )}
      </motion.div>
    </>
  );
};

const CardRender = ({ height = '100%', width = '50%' }) => {
  return (
    <motion.div
      style={{ height: height || '100%' }}
      className={`m-4 bg-yellow-300 w-[${width}] border-2 border-black rounded-3xl text-center text-black text-xl p-4`}
    >
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores ipsa optio delectus iste tempore officia quaerat
      sapiente nobis necessitatibus.
    </motion.div>
  );
};

const Section = ({ i, children, isCenter = true, scrollPosi }) => {
  const ref = useRef(null);
  const isInView = useInView(ref, { once: true });
  const controls1 = useAnimation();

  useEffect(() => {
    if (isInView) {
      controls1.start('visible');
    }
  }, [isInView]);

  let divVariant = {
    hidden: { y: '100vh', opacity: 0 },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        delay: 1,
        duration: 2,
        type: 'tween',
      },
    },
  };

  return (
    <motion.section
      style={{
        ...(i == 10 && { top: 50 }),
        ...(i == 10 && { height: '100%' }),
      }}
      className={`relative grid${i + 1} ${isCenter ? 'center' : ''} ${(i + 1) % 2 == 0 ? '' : 'margin'}`}
      ref={ref}
    >
      <motion.div
        className="relative h-[100%]"
        variants={divVariant}
        initial="hidden"
        animate={isInView ? 'visible' : ''}
      >
        {children}
      </motion.div>
    </motion.section>
  );
};

export default ScrollAnimate;
